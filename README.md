# epub2gpub
## Simple tool to convert EPUB to Gempub for reading in gemini

### What's Gempub?
[Gempub](https://codeberg.org/oppenlab/gempub) is an ebook format suitable for
hosting in [gemini space](https://gemini.circumlunar.space/)

### This sucks.
Probably. It's a result of pounding out some amateur Nim code on the last day
of my winter vacation. It's mostly useful, and spits out a gpub that is at
least useable in LaGrange.

