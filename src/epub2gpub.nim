import commandant
import html2gem
import nim_epub/epub
import zippy/ziparchives
import std/[os, strformat, strutils]

# This assumes a Standard Ebooks "compatible epub" layout:
# https://standardebooks.org/manual/1.7.0/single-page

# This is almost certainly broken on anything that doesn't conform
# to their standard.

commandline:
  argument(srcbook, string)
  errormsg("\nepub2gpub <compatible.epub>")

var epubdir: string
var gpubdir: string

proc getTitle(src: EPUBPackage): string =
  for title in src.metadata.titles:
    case title.id
    of "title":
      result = title.value
      break
    else:
      continue

proc getAuthor(src: EPUBPackage): string =
  for creator in src.metadata.creators:
    case creator.id
    of "author":
      result = creator.value
      break
    else:
      continue

proc getCover(src: EPUBPackage): string =
  var coverpath: string
  for item in src.manifest.items:
    case item.id
    of "cover.jpg":
      coverpath = item.href
      break
    else:
      continue
  let (head, tail) = splitPath(coverpath)
  copyFile(epubdir / "epub" / coverpath, gpubdir / tail)
  result = tail
  
proc createMetadata(src: EPUBPackage) =
  var md = open(gpubdir / "metadata.txt", fmWrite)

  md.writeLine(fmt"title: {src.getTitle}")
  md.writeLine("gpubVersion: 1.0.0")
  md.writeLine(fmt"cover: {src.getCover}")
  md.writeLine("index: book/index.gmi")
  md.writeLine(fmt"author: {src.getAuthor}")
  md.writeLine(fmt"language: {src.lang}")
  md.writeLine("charset: UTF-8")
  # TODO: publishing date stuff
  md.writeLine(fmt"license: {src.metadata.rights.value}")
  md.writeLine(fmt"version: {src.version}")
  md.close

proc convertText(src: EPUBPackage) =
  createDir(gpubdir / "book")
  
  var index = open(gpubdir / "book" / "index.gmi", fmWrite)
  
  for entry in src.spine.itemrefs:
    let (dir, name, ext) = splitFile(entry.idref)
    let (title, gemini) = toGemini(epubdir / "epub/text" / entry.idref)

    if gemini == "\n":  # section headers will sometimes be empty
      index.writeLine(&"### {title}")
      continue

    let chapterfile = &"{name}.gmi"
    index.writeLine(&"=> {chapterfile} {title}")

    var chapter = open(gpubdir / "book" / chapterfile, fmWrite)
    chapter.write(gemini)
    chapter.close
  index.close

proc createGpub(src: EPUBPackage): string =
  let author = getAuthor(src).toLower.replace(" ", "_")
  let title = getTitle(src).toLower.replace(" ", "_")
  result = &"{author}_{title}.gpub"
  gpubdir = &"{result}.unpacked"

  if dirExists(gpubdir):
    echo fmt"{gpubdir} already exists"
    quit(1)
  createDir(gpubdir)
  src.createMetadata
  src.convertText

when isMainModule:
  epubdir = fmt"{srcbook}.unpacked"

  if not dirExists(epubdir):
    extractAll(srcbook, epubdir)
    
  let epubsrc = parsePackageDocument(epubdir)
  let book = epubsrc.createGpub
  createZipArchive(&"{gpubdir}/", book)

  removeDir(epubdir)
  removeDir(gpubdir)
