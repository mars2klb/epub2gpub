import std/[parsexml, strformat, strutils, xmlparser, xmltree]

proc sanitized(n: XmlNode, toskip: string = "%SKIP%"): string =
  result = ""
  for item in n.items:
    case item.kind
    of xnElement:
      if item.tag == "br":
        result.add(" ")
      if item.tag == "a":
        if item.attr("class") == "epub-type-noteref":
          result.add(sanitized(item, item.innerText))
          continue
      result.add(sanitized(item))
    else:
      if item.innerText != toskip:
        result.add(item.innerText)
      else:
        result.add(" ")

proc convert(body: XmlNode): string =
  var gemtext: string

  for item in body.items:
    case item.tag
    of "p":
      gemtext.add(&"{item.sanitized}\n")
    of "h1":
      gemtext.add(&"# {item.innerText}\n")
    of "h2":
      gemtext.add(&"## {item.innerText}\n")
    of "h3":
      gemtext.add(&"### {item.innerText}\n")
    of "hr":
      gemtext.add("------\n")
    gemtext.add("\n")

  result = gemtext

proc getTitle(head: XmlNode): string =
  for item in head.items:
    case item.tag.toLower
    of "title":
      result = &"{item.innerText}"

proc toGemini*(html: string): (string, string) =
  var errors = newSeq[string](0)
  let xml = loadXml(html, errors, {allowEmptyAttribs})

  var gemini: string
  var title: string

  if errors.len > 0:
    echo fmt"warning: {errors}"

  for item in xml.items:
    case item.kind:
      of xnElement:
        case item.tag.toLower
        of "body":
          if item.child("article") != nil:
            gemini = convert(item.child("article"))
          elif item.child("section") != nil:
            gemini = convert(item.child("section"))
        of "head":
          if item.child("title") != nil:
            title = getTitle(item)
      else:
        continue

  return (title, gemini)
  
