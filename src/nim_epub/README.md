https://github.com/achesak/nim-epub

About
=====

nim-epub is a Nim module for parsing EPUB documents.

Run `nim doc epub.nim` for usage and more info.

License
=======

nim-epub is released under the MIT open source license.

