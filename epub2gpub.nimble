# Package

version       = "0.3"
author        = "McKay Marston"
description   = "Convert epub to gpub (https://codeberg.org/oppenlab/gempub)"
license       = "MIT"
srcDir        = "src"
bin           = @["epub2gpub"]


# Dependencies
requires "nim >= 1.6.10"
requires "zippy >= 0.10.5"
requires "commandant >= 0.15.0"
